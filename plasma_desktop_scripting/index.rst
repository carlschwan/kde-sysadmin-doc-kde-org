Plasma Desktop Scripting
=====================

.. toctree::
   :titlesonly:
   :hidden:

   examples
   keys

ECMA Script Interaction With Plasma Shells
------------------------------------------

It is possible to control and interact with a Plasma user interface shell such as a plasma-desktop or (starting in KDE SC 4.5) plasma-netbook session using ECMA Script (aka JavaScript). This scripting mechanism exposes containments (Desktop Activities and Panels), widgets and various other aspects of plasma-desktop configuration using the widely known and used ECMA Script language. The QtScript engine is used for the runtime environment.

This document describes the API that is provided along with how to run such scripts in plasma-desktop.

:doc:`examples`
~~~~~~~~

Visit :doc:`examples` for more examples. Here's a highlight:

Advanced example: Adding a widget to the System Tray
____________________________________________________


.. code-block:: javascript

    const widgetName = "org.kde.plasma.printmanager";

    panelIds.forEach((panel) => { //search through the panels
        panel = panelById(panel);
        if (!panel) {
            return;
        }
        panel.widgetIds.forEach((appletWidget) => {
            appletWidget = panel.widgetById(appletWidget);

            if (appletWidget.type === "org.kde.plasma.systemtray") {
                systemtrayId = appletWidget.readConfig("SystrayContainmentId");
                if (systemtrayId) {
                   print("systemtray id: " + systemtrayId)
                   const systray = desktopById(systemtrayId);
                   systray.currentConfigGroup = ["General"];
                   const extraItems = systray.readConfig("extraItems").split(",");
                   if (extraItems.indexOf(widgetName) === -1) {
                       extraItems.push(widgetName)
                       systray.writeConfig("extraItems", extraItems);
                       systray.reloadConfig();
                   }
                }
            }
        });
    });


Running Scripts
~~~~~~~~~~~~~~~

There are three ways that scripts can be executed in plasma-desktop:

-  **on first run**: when plasma-desktop is started without any
   pre-existing configuration, any scripts in
   $APPDATA/plasma-desktop/init/ with a ".js" suffix are run. If there
   is more than one script, they are run sequentially in the
   alphabetical order of the file names.

.. note::

   For security reasons, scripts located in the user's home directory will '''not''' be run during this phase.

-  **on update**: when plasma-desktop is started, it will check in

.. code:: bash

   `kf5-config --path data`/plasma-desktop/updates/

with a ".js" suffix for scripts that have not yet been run. If there is
more than one script which has not been run yet they will be executed
serially in the alphabetical order of the file names.

A record of which update scripts have been run is kept in the
application's config file in the [Updates] group. This means that if the
plasma-desktop configuration file is removed, all the update scripts
will be run again.

.. note::

   For security reasons, scripts located in the user's home directory will '''not''' be run during this phase.

-  **interactively**: an interactive scripting dialog can be requested
   either via the KRunner window (Alt+F2, by default, or via the "Run
   Command" entry in various desktop menus) by entering "desktop
   console" as the search term. It can also be triggered directly via
   dbus with

   .. code:: bash

      qdbus-qt5 org.kde.plasmashell /PlasmaShell org.kde.PlasmaShell.showInteractiveConsole

ECMA Script may be entered directly into this window for execution
and output appears in the lower half of the window. Ctrl+E is a
shortcut to run scripts, and scripts can be saved to and loaded from
disk.

Scripts from files can also be loaded using KRunner with "desktop
console /path/to/file" or via dbus with

.. code:: bash

   qdbus-qt5 org.kde.plasma-desktop /MainApplication loadScriptInInteractiveConsole /path/to/file

Templates
---------

Templates are named packages that contain scripts. This provides a way
for common functionality to be easily reused, helping to increase
consistency and lower maintenance costs. Templates can be loaded from
other scripts by name and they are also used to populate some parts of
the user interface, such as the entries in the Add Panels menu.

A template is a small set of files in a specified file hierarchy (or, in
Plasma terms, a "Package"). In particular, a Template package contains
the following files:

-  metadata.desktop: a .desktop file describing the template
-  contents/layout.js: a Javascript file containing the actual script

Templates are stored under share/apps/plasma/layout-templates and may be
installed using \`plasmapkg -t layout-template -i /path/to/package`.
Template packages may also be provided as a .zip file with a
.plasmalayout suffix.

The metadata.desktop file contains the usual .desktop entries such as
Name and Icon but must also contain ``Type=Service`` and
``ServiceTypes=Plasma/LayoutTemplate`` entries. If the layout is specific to
a given Plasma application, such as plasma-desktop, this can be specific
using ``X-Plasma-Shell``. ``X-Plasma-ContainmentCategories`` defines what kind
of layout it is with possible values being panel and desktop. Finally a
``X-KDE-PluginInfo-Name`` entry is required to provide a globally unique
internal name for the Template. Here is an example of a Template that
provides a Panel layout for Plasma Netbook:

.. code:: ini

   [Desktop Entry]
   Encoding=UTF-8
   Name=Cool Panel
   Type=Service
   ServiceTypes=Plasma/LayoutTemplate
   X-Plasma-Shell=plasma-netbook
   X-Plasma-ContainmentCategories=panel
   X-KDE-PluginInfo-Author=Aaron Seigo
   X-KDE-PluginInfo-Email=aseigo@kde.org
   X-KDE-PluginInfo-Name=org.kde.CoolNetbookPanel
   X-KDE-PluginInfo-Version=1.0
   X-KDE-PluginInfo-Website=http://plasma.kde.org/
   X-KDE-PluginInfo-Category=
   X-KDE-PluginInfo-Depends=
   X-KDE-PluginInfo-License=GPL
   X-KDE-PluginInfo-EnabledByDefault=true

When running a template, two global variables will be accessible in
read-only mode: templateName and templateComment. They will contain the
Name and Comment fields of the above desktop file, and are translated if
a localization is available.

.. _examples_of_usage:

Examples of Usage
~~~~~~~~~~~~~~~~~

.. _creating_panels:

Creating panels
^^^^^^^^^^^^^^^

A good example of the use of templates is the use case that triggered
the creation of this feature: the desire to make it easy for users to
re-create the default panel that is created on the first start. There is
a Template called org.kde.plasma-desktop.defaultPanel that ships with
the KDE Plasma Workspace which contains the layout for the initial
default panel. This is referenced by the default Plasma Desktop init
script and because it is marked as a Panel Template in the
metadata.desktop file it also shows up to the user in the Add Panels
menu. When selected by the user from the menu, the exact same panel that
is created on desktop start up is created for them, complete with Plasma
Widgets and configuration.

.. _automating_tasks:

Automating tasks
^^^^^^^^^^^^^^^^

Another example of the usefulness of templates is the "Find Widgets"
template. This template, which first shipped with Plasma Desktop v4.5,
provides a function for finding widgets by name. It appears in the
toolbar "Load" and "Use" menus in the Desktop Console in plasma-desktop,
and makes finding widgets as simple as:

.. code:: javascript

   const template = loadTemplate('org.kde.plasma-desktop.findWidgets');
   template.findWidgets('systemtray');

Since just finding the widget is not enough, you can connect a callback
to do additional operations, such as removing the widget :

.. code:: javascript

   removeWidget = function(widget, containment) {
     widget.remove();
   }

   const template = loadTemplate('org.kde.plasma-desktop.findWidgets');
   template.findWidgets('systemtray', removeWidget);

.. _activity_templates:

Activity templates
^^^^^^^^^^^^^^^^^^

Probably the most user visible use of templates are "Activity
templates". The structure of Activity templates is similar to the other
use of templates, but a few extra features are provided in the
metadata.desktop file. Here is an example of such an activity template:

.. code:: ini

   [Desktop Entry]
   Encoding=UTF-8
   Name=Cool Activity Template
   Icon=user-desktop
   Type=Service
   ServiceTypes=Plasma/LayoutTemplate
   X-Plasma-Shell=plasma-desktop
   X-Plasma-ContainmentCategories=desktop
   X-Plasma-ContainmentLayout-ExecuteOnCreation=dolphin $desktop, gwenview $pictures
   X-Plasma-ContainmentLayout-ShowAsExisting=true
   X-KDE-PluginInfo-Author=John Doe
   X-KDE-PluginInfo-Email=john@doe.org
   X-KDE-PluginInfo-Name=org.kde.plasma-desktop.CoolTemplate
   X-KDE-PluginInfo-Version=1.0
   X-KDE-PluginInfo-Website=http://john.doe.org
   X-KDE-PluginInfo-Category=
   X-KDE-PluginInfo-Depends=
   X-KDE-PluginInfo-License=GPL
   X-KDE-PluginInfo-EnabledByDefault=true

The layout itself is still created from the layout.js file as usual, but
this template also shows as a precreated activity to the user thanks to
the ``X-Plasma-ContainmentLayout-ShowAsExisting`` key. Additionally, it
starts applications in the newly created activity using the
``X-Plasma-ContainmentLayout-ExecuteOnCreation`` key.

That key is a list of commands to execute, and it supports the following
variables:

-  $desktop
-  $autostart
-  $documents
-  $music
-  $video
-  $downloads
-  $pictures

They all expand into the path toward the user corresponding default
folder.

.. _update_scripts:

Update scripts
--------------

Update javascript scripts can be added per-shell at the location

.. code:: bash

    share/plasma/shells/org.kde.plasma.desktop/updates/

The API available from those config files is the same available from the
normal init.js file. from there is possible to add or remove applets or
modify config values.

.. _look_and_feel_dependent_default_setup_for_applets:

Look and Feel dependent default setup for applets
-------------------------------------------------

The look and feel package can contain JavaScript files to override the
default configuration for applets, if distributions or system
integrators want to have a different default setup. The JS files are
located under the L&F package folder, for instance

.. code:: bash

/opt/kde5/share/plasma/look-and-feel/org.kde.breeze.desktop/contents/plasmoidsetupscripts/org.kde.plasma.analogclock.js

The API available is the same as the init.js script, with the addition
of the global variables **applet** and **containment** that point to the
instance of the applet and the instance of the containment the applet is
in. In the case of a containment, the variables **applet** and
**containment** will be the same.

API
---

In addition to the normal ECMA Script API and the Qt-specific extensions
(such as signal/slot support) provided by QtScript, the following API is
provided for use by scripts.

All of the API below, unless otherwise noted with a version noticed,
appear as below in the KDE Software Compilation v4.4.0 and later. API
that is not noted as being part of a given class or object is part of
the global namespace.

.. note::

   API compatibility is guaranteed from version to version starting with KDE Software Compilation v4.4.0.

.. _version_numbers:

Version Numbers
~~~~~~~~~~~~~~~

Starting with KDE SC 4.5, the version number of both the scripting API
and the application is available to the script via the following
read-only properties:

-  *String* **applicationVersion**: the version of the application, e.g.
   0.3
-  *String* **platformVersion**: the version of the KDE Platform, e.g.
   0.3
-  *number* **scriptingVersion**: the version of the scripting API; e.g.
   in KDE SC 4.5 this is 2

.. _activities_and_desktops:

Activities and desktops
~~~~~~~~~~~~~~~~~~~~~~~

**Desktop** are the desktop layer in a plasma-desktop session and may
contain widgets. In slightly more technical terms, they are desktop
containments. Activities can be created, enumerated, modified and
destroyed.

New Activities can be created using the **newActivity** function, like
this:

.. code:: javascript

   let activityId = newActivity("org.kde.plasma.folderview")

it returns the activity string Id (to not be confused from the numerical
id of the Deskop object) The string passed into the constructor maps to
the ``X-KDE-PluginInfo-Name=`` entry in the plugin's .desktop file). See the
documentation on the Containment object class below.

Read-only properties:

-  *Array[number]* **activityIds**: returns a list of integer ids of all
   existing Plasma activities
-  *Array[String]* **knownActivityTypes**: (scripting version >= 2) a
   list of types of activities that can be created. This is useful to
   check if an Activity type is available on the system before trying to
   construct one.

Functions:

-  *Array[Desktop]* **desktopsForActivity(string id)**: return all the
   desktops associated to a specific activity id (one per phisical
   screen)
-  *Desktop* **desktopById(number id)**: return an object representing
   the activity with the given id
-  *Desktop* **desktopForScreen(number screen[, number dekstop])**:
   returns an object representing the activity currently associated with
   the given screen and, optionally, the given desktop.
-  *Array[Desktop]* **desktops()**: returns an array of all desktops
   that currently exist
-  *Array[string]* **activities()**: returns an array of all activity
   ids of the activities that currently exist

Panels
~~~~~~

Panels can be created, enumerated, modified and destroyed. A panel
object combines both a containment as well as the container itself,
allowing for full control of things such as where it appears on the
screen and the hiding features associated with them.

New Panels can be created using the Panel constructor, like this:

.. code:: javascript

   let panel = new Panel("dock`")

The string passed into the constructor maps to the
X-KDE-PluginInfo-Name= entry in the plugin's .desktop file).

Read-only properties:

-  *Array[number]* **panelIds**: returns a list of integer ids of all
   existing Plasma panels
-  *Array[String]* **knownPanelTypes**: (scripting version >= 2) a list
   of types of panels that can be created. This is useful to check if a
   Panel type is available on the system before trying to construct one.

Functions:

-  *Panel* **panelById(int id)**: returns an object representing the
   Panel that matches the given id
-  *Array[Panels]* **panels()**: returns an array of all panels that
   currently exist

.. _activities_and_panels:

Activities and Panels
~~~~~~~~~~~~~~~~~~~~~

Activity and Panel objects, once created by the script, or as returned
by activityById, activityForScreen, or panelById) provide the following
read-only properties:

-  *number* '''id: the integer id of this activity
-  *String* **formFactor**: returns the form factor of the activity,
   e.g. "planar" for most desktop activities,"mediacenter" for media
   centers and either "horizontal" or "vertical" for panels.
-  *Array[number]* **widgetIds**: a list of integer ids of all the
   widgets in this Activity
-  *Array[String]* **configKeys**: (scriptingVersion >= 2) a list of all
   keys that are set in the current configuration group
-  *Array[String]* **configGroups**: (scriptingVersion >= 2) a list of
   all the groups in the current configuration group
-  *Array[String]* **globalConfigKeys**: (scriptingVersion >= 2) a list
   of all keys that are set in the current global configuration group
-  *Array[String]* **globalConfigGroups**: (scriptingVersion >= 2) a
   list of all the groups in the current global configuration group

as well as the following read/write properties:

-  *number* **desktop**: the virtual desktop this activity is associated
   with, or -1 for none
-  *number* **screen**: the screen this activity is associated with, or
   -1 for none
-  *String* **name**: the name of this activity
-  *String* **wallpaperPlugin**: (scriptingVersion >= 2) the wallpaper
   plugin to use with the Activity
-  *String* **wallpaperMode**: (scriptingVersion >= 2) the wallpaper
   plugin mode to use with the Activity
-  *Array[String]* **currentConfigGroup**: (scriptingVersion >= 2) the
   current configuration group path, with each entry in the array
   representing a sub-group. This allows one to access trees of groups
   with code such as: widget.currentConfigGroup = new Array('topGroup',
   'subGroupOfTopGroup'). An empty Array means the default (top-level)
   configuration group for the widget
-  *String* **version**: (scriptingVersion >= 2) the version of the
   Activity or Panel

and the following methods:

-  **remove()**: deletes this activity and all widgets inside of it
-  *Widget* **widgetById(number id)**: returns an object representing
   the widget with the given id
-  *Widget* **addWidget(String name)**: adds a new widget to the
   activity; the name maps to the ``X-KDE-PluginInfo-Name=`` entry in
   the widget's .desktop file
-  *Widget* **addWidget(Widget widget)**: adds an existing widget to
   this activity; useful for moving widgets between Activities and
   Panels
-  **showConfigurationInteface()**: shows the configuration user
   interface for this Activity or Panel on the screen
-  **readConfig(String key, any default)**: (scriptingVersion >= 2)
   reads the value of key in the config with default for the default
   value
-  **writeConfig(String key, any value)**: (scriptingVersion >= 2) sets
   key to value in the config
-  **readGlobalConfig(String key, any default)**: (scriptingVersion >=
   2) reads the value of key in the global config with default for the
   default value
-  **writeGlobalConfig(String key, any value)**: (scriptingVersion >= 2)
   sets key to value in the global config
-  **reloadConfig()**: (scriptingVersion >= 2) causes the Activity or
   Panel to reload its configuration; reaction to configuration changes
   made using readConfig are usually activated on script exit, but this
   can be triggered earlier on a per-widget basis using this method
-  *Array[String]* **currentGlobalConfigGroup**: (scriptingVersion >= 2)
   the current global configuration group path, with each entry in the
   array representing a sub-group, similar to currentConfigGroup.
   However, global configuration is shared by all instances of panels
   and activities of the same type.
-  *Array[Widget]* **widgets([String type])**: (scriptingVersion >= 2)
   returns all the widgets in the Panel or Activity. If the optional
   type is specified, only widgets matching that type will be returned.

In addition to all of the above properties and functions, Panel objects
also provide the folowing read/write properties:

-  *number* **length**: the number of pixels along the screen edge used
-  *number* **minimumLength**: (scriptingVersion >= 7) the minimum
   number of pixels along the screen edge used (auto-resize panels)
-  *number* **maximumLength**: (scriptingVersion >= 7) the maximum
   number of pixels along the screen edge used (auto-resize panels)
-  *number* **height**: the height (or for vertical panels, the width)
   of the panel
-  *String* **hiding**: the hiding mode of the panel, one of "none" (for
   no hiding), "autohide", "windowscover" or "windowsbelow"
-  *String* **alignment**: right, left or center alignment of the panel
   (for vertical panels, right corrsponds to top and left to bottom)
-  *String* **location**: returns the location of the activity (only
   relevant for Panels); valid values include "top", "bottom", "left",
   "right" and "floating"

Widgets
~~~~~~~

Widgets may be enumerated by calling the widgetIds property on an
Activity or Panel object. With a widget id in hand, a Widget object can
be retrieved by calling widgetById(id) on an Activity or Panel object.
New Widgets can be created with add addWidget(String) function provided
by Activity and Panel objects.

.. _checking_if_a_widget_is_installed:

Checking if a widget is installed
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A list of all installed widget types can be retrieved the following
read-only property:

-  *Array[String]* **knownWidgetTypes** (scripting version >= 2)

This can be used most conveniently with the indexOf() method, like this:

.. code:: javascript

   if (knownWidgetTypes.indexOf('someWidgetPluginName') > -1) {
       print("It is installed on this system!");
   } else {
       print("It is not installed :(");
   }

.. _widget_object_api:

Widget Object API
^^^^^^^^^^^^^^^^^

A Widget object provides the following read-only properties:

-  *number* **id**: the id of the widget
-  *String* **type**: the plugin type of this widget
-  *Array[String]* **configKeys**: a list of all keys that are set in
   the current configuration
-  *Array[String]* **configGroups**: a list of all the groups in the
   current configuration
-  *Array[String]* **globalConfigKeys**: (scriptingVersion >= 2) a list
   of all keys that are set in the current global configuration group
-  *Array[String]* **globalConfigGroups**: (scriptingVersion >= 2) a
   list of all the groups in the current global configuration group
-  *String* **version**: (scriptingVersion >= 2) the version of the
   Activity or Panel

as well as the following read-write properties:

-  *Array[String]* **currentConfigGroup**: the current configuration
   group path, with each entry in the array representing a sub-group.
   This allows one to access trees of groups with code such as:
   widget.currentConfigGroup = new Array('topGroup',
   'subGroupOfTopGroup'). An empty Array means the default (top-level)
   configuration group for the widget
-  *Array[String]* **currentGlobalConfigGroup**: (scriptingVersion >= 2)
   the current global configuration group path, with each entry in the
   array representing a sub-group, similar to currentConfigGroup.
   However, global configuration is shared by all instances of widgets
   of the same type.
-  *QRectF* **geometry**: the geometry of the widget (settable)
-  *String* **globalShortcut**: the shortcut sequence (in the format
   used by QKeySequence, e.g. "Alt+F1") associated with this widget
-  *number* **index**: the layout index of the widget; in a Panel this
   corresponds to the order the widget appears in. Changing the value of
   the index will change the position of the widget in Panels and may do
   so in some Activities as well.

and the following methods:

-  **remove()**: deletes this widget
-  **readConfig(String key, any default)**: reads the value of key in
   the config with default for the default value
-  **writeConfig(String key, any value)**: sets key to value in the
   config
-  **readGlobalConfig(String key, any default)**: (scriptingVersion >=
   2) reads the value of key in the global config with default for the
   default value
-  **writeGlobalConfig(String key, any value)**: (scriptingVersion >= 2)
   sets key to value in the global config
-  **reloadConfig()**: causes the widget to reload its configuration;
   reaction to configuration changes made using readConfig are usually
   activated on script exit, but this can be triggered earlier on a
   per-widget basis using this method
-  **showConfigurationInteface()**: shows the configuration user
   interface for this widget on the screen

.. _screen_geometry:

Screen Geometry
~~~~~~~~~~~~~~~

Read-only properties:

-  *number* **screenCount**: returns the number of screens connected to
   the computer

Functions:

-  *QRectF* **screenGeometry(number screen)**: returns a rect object
   representing the geometry of a screen

.. _wallpaper_plugins:

Wallpaper Plugins
~~~~~~~~~~~~~~~~~

-  *Array[String => Array[String]]* **knownWallpaperPlugins()**:
   (scripting version >= 4) returns a list of all installed wallpaper
   plugins. The keys of the array are the wallpaper plugin names. The
   values are arrays containing the modes available for that wallpaper
   plugin. The mode array may be empty, as most wallpaper plugins only
   offer one mode.

.. _locating_applications_and_paths:

Locating Applications and Paths
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  *boolean* **applicationExists(String name)**: (scripting version >=
   4) searches $PATH first, then tries in the application menu system by
   application storage name (aka the .desktop file name), then Name=
   entries for apps with installed .desktop files, then GenericName=
   entries for same
-  *mixed* **defaultApplication(String kind [, boolean storageId =
   false])**: (scripting version >= 4) returns the executable (or if
   storageId is true, then the app menu system id, e.g. its .desktop
   file name) of the default app. The "kind" parameter may be a
   well-known application type including "browser", "mailer",
   "filemanager", "terminal", "imClient" and "windowmanager" (or any
   other entry in share/apps/kcm_componentchooser/kcm_*.desktop); it may
   also be a mimetype (e.g. "application/pdf"). On failure, it returns
   false.
-  *String* **applicationPath(String name)**: (scripting version >= 4)
   returns the full local path to a given application or .desktop file
   if it exists. Example:

.. code:: javascript

   const desktopfile = "firefox.desktop"
   const executable  = "firefox"
   if (applicationExists(executable)) {  
       print(`${executable} exists with this path: ${applicationPath(executable)}`)
       print(`${executable} .desktop file is located here : ${applicationPath(desktopfile)}`)
   } else{
       print(`${executable} does not exist`)
   }

-  *String* **userDataPath([String type, String path])**: (scripting
   version >= 4) returns the default path for user data. Called with no
   parameters, it returns the user's home directory. If only one string
   is passed in, the standard directory for that type of data in the
   user's home directory will be located; the following values are
   recognized:

   -  documents
   -  music
   -  video
   -  downloads
   -  pictures
   -  autostart
   -  desktop (should be considered deprecated for Plasma workspaces)

If a second string is passed in, it is considered a request for a
specific path and the following types are recognized:

-  

   -  apps - Applications menu (.desktop files).
   -  autostart - Autostart directories (both XDG and kde-specific)
   -  cache - Cached information (e.g. favicons, web-pages)
   -  cgi - CGIs to run from kdehelp.
   -  config - Configuration files.
   -  data - Where applications store data.
   -  emoticons - Emoticons themes
   -  exe - Executables in $prefix/bin. findExe() for a function that
      takes $PATH into account.
   -  html - HTML documentation.
   -  icon - Icons, see KIconLoader.
   -  kcfg - KConfigXT config files.
   -  lib - Libraries.
   -  locale - Translation files for KLocale.
   -  mime - Mime types defined by KDE-specific .desktop files.
   -  module - Module (dynamically loaded library).
   -  qtplugins - Qt plugins (dynamically loaded objects for Qt)
   -  services - Services.
   -  servicetypes - Service types.
   -  sound - Application sounds.
   -  templates - Templates for the "Create new file" functionality.
   -  wallpaper - Wallpapers.
   -  tmp - Temporary files (specific for both current host and current
      user)
   -  socket - UNIX Sockets (specific for both current host and current
      user)
   -  xdgconf-menu - Freedesktop.org standard location for menu layout
      (.menu) files.
   -  xdgdata-apps - Freedesktop.org standard location for application
      desktop files.
   -  xdgdata-dirs - Freedesktop.org standard location for menu
      descriptions (.directory files).
   -  xdgdata-mime - Freedesktop.org standard location for MIME type
      definitions.
   -  xdgdata-icon - Freedesktop.org standard location for icons.
   -  xdgdata-pixmap - Gnome-compatibility location for pixmaps.

The second parameter should be a specific resource to find the path to.
An example might be userDataPath("data", "plasma-desktop").

.. _external_configuration_files:

External Configuration Files
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Scripting version >=6

Access to configuration files outside of the application's appletsrc
file is provided by the ConfigFile object which has the following
constructors:

-  *ConfigFile(ConfigFile other)*: creates a new ConfigFile object with
   the first as its parent, which automatically makes all groups of this
   object child groups of that parent
-  *ConfigFile(string file[, string group])*: creates a new ConfigFile
   object for file and optionally set to group group

ConfigFile has the following functions:

-  *writeEntry(string key, any value)*: writes value for key in the
   current group
-  *readEntry(string key)*: returns the value of key in the current
   group
-  *deleteEntry(key)*: deletes key in the current group

ConfigFile has the following read/write properties:

-  *file*: the name of the configuration file. Maybe an absolute path or
   just a file name to be located automatically in the system's
   configuration repository.
-  *group*: the name of the current group. To get to child groups,
   create a new ConfigFile with another ConfigFile as its parent.

ConfigFile has the follow read-only properties:

-  *entryList*: all the keys in the current group
-  *groupList*: all the child groups in the current group

Example usage:

.. code:: javascript

   // open the kickoffrc file
   const config = ConfigFile('kickoffrc');
   // switch to the RecentlyUsed group
   config.group = 'RecentlyUsed';
   // write an entry into it
   config.writeEntry('MaxApplications', 25);

   // now put Yes=20 into RecentlyUsed/Test
   const config2 = ConfigFile(config, 'Test);
   config2.writeEntry('Yes', 20);

.. _misc._global_properties_and_functions:

Misc. Global Properties and Functions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Read-write properties:

-  *boolean* **locked**: whether the desktop shell and widgets are
   locked or not (settable)
-  *string* **theme**: (scripting version >= 3) the name of the desktop
   theme to use for the interface, e.g. default, Air, Oxygen, etc.

Read-only properties:

-  *boolean* **hasBattery**: whether or not the system has the ability
   to run on battery power, e.g. a laptop or mobile device
-  *boolean* **multihead**: (scripting version >= 3) true if the system
   is running with multiple screens in a "Xaphod" multiple display
   server configuration
-  *int* **multiheadScreen**: (scripting version >= 3) if multihead is
   true, contains the (real) screen id of the current screen

Functions:

-  **sleep(number ms)**: sleeps the script for the specified number of
   milliseconds

QRectF
~~~~~~

A rectangle class is also provided for use with Widget, Panel and screen
geometry properties and functions.

Read-only properites:

-  *boolean* **empty**: true if the rectangle's width or height is less
   than, or equal to, 0; an empty rectangle is also invalid
-  *boolean* **null**: true if the rectangle has both the width and the
   height set to 0; a null rectangle is also empty and not valid
-  *boolean* **valid**: true if the rectangle has a width > 0 and height
   0.

Read-write properties:

-  *number* **left**
-  *number* **top**
-  *number* **bottom**
-  *number* **right**
-  *number* **height**
-  *number* **width**
-  *number* **x**
-  *number* **y**

Constructors:

-  **QRectF**
-  **QRectF(number x, number y, number width, number height)**: Sets the
   coordinates of the rectangle's top-left corner to (x, y), and its
   size to the given width and height.

Functions:

-  **adjust(number dx1, number dy1, number dx2, number dy2)**: adds dx1,
   dy1, dx2 and dy2 respectively to the existing coordinates of the
   rectangle
-  *QRectF* **adjusted(number dx1, number dy1, number dx2, number
   dy2)**: returns a new QRectF with dx1, dy1, dx2 and dy2 added
   respectively to the existing coordinates of the rectangle
-  **translate(number dx, number dy)**: translates the rect by dx, dy
-  **setCoords(number x1, number y1, number x2, number y2)**: sets the
   coordinates of the rectangle's top-left corner to (x1, y1), and the
   coordinates of its bottom-right corner to (x2, y2).
-  **setRect(number x, number y, number width, number height)**: sets
   the coordinates of the rectangle's top-left corner to (x, y), and its
   size to the given width and height.
-  *boolean* **contains(number x, number y)**: returns true if the rect
   contains the point (x, y)
-  *'moveBottom(number delta)*: moves the bottom by delta pixels
-  **moveLeft(number delta)**: moves the left by delta pixels
-  **moveRight(number delta)**: moves the right by delta pixels
-  **moveTo(number x, number y)**: moves the top left of the rect to
   point (x, y)
-  **moveTop(number delta)**: moves the top by delta pixels

:doc:`keys`
-----------

Here you find a list of commonly used configuration keys to use with the writeConfig command. Where the documentation notes that a key is in a subgroup other than General, remember to first use currentConfigGroup.
