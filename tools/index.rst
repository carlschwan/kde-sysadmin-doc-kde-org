Tools
=====

.. toctree::
   :titlesonly:
   :hidden:

   Desktop Sharing <https://docs.kde.org/trunk5/en/kdenetwork/krfb/index.html>
   Shell Scripting with KDE Dialogs <https://techbase.kde.org/Development/Tutorials/Shell_Scripting_with_KDE_Dialogs>
   controlling_access_content_get_new_stuff
