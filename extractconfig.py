#!/bin/env python3

import os
import sys
import xml.dom.minidom
from xml.dom.minidom import parse
import configparser

########
# This application loops through installed applets and extracts the config XML file
# Output is then presented in mediawiki format for copying and pasting to https://userbase.kde.org/KDE_System_Administration/PlasmaDesktopScripting#Configuration_Keys

# app should be kept flexible enough to port to a different format in future


#set plasmoid installation path manually if applicable

DEBUG=False

def parseProjectDir(projectDir, f):
    plasmoids = os.listdir(projectDir)
    plasmoids.sort()
    for plasmoid in plasmoids:
        path = projectDir + "/" + plasmoid
        parseConfig(path, plasmoid, f)


def nameForId(path, plasmoid):
    configPath = ""
    if os.path.exists(path + '/metadata.desktop'):
        configPath = path + '/metadata.desktop'
    elif os.path.exists(path + '/package/metadata.desktop'):
        configPath = path + '/package/metadata.desktop'
    else:
        return "BUG"

    config = configparser.ConfigParser()
    config.read(configPath)

    return config['Desktop Entry']['X-KDE-PluginInfo-Name']


def parseConfig(path, plasmoid, f):
    if plasmoid == "CMakeLists.txt" or plasmoid == "Mainpage.dox":
        return
    configPath = ""
    if os.path.exists(path + "/contents/config/main.xml"):
        configPath = "/contents/config/main.xml"
    elif os.path.exists(path + "/package/contents/config/main.xml"):
        configPath = "/package/contents/config/main.xml"
    try:
        dom = xml.dom.minidom.parse(path + configPath).documentElement

        print("", file=f)
        print(nameForId(path, plasmoid), file=f)
        print("----------------------------------------------------------------", file=f)
        for group in dom.getElementsByTagName("group"):
            groupName = group.getAttribute("name")
            print("", file=f)
            print(groupName, file=f)
            print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~", file=f)
            print("", file=f)

            for entry in group.getElementsByTagName("entry"):
                name = entry.getAttribute("name")
                type = entry.getAttribute("type")
                default = ""
                description = ""

                if entry.hasAttribute("hidden") and entry.getAttribute("hidden") == "true":
                    continue

                defaultTags = entry.getElementsByTagName("default")
                if (defaultTags.length > 0 and defaultTags[0].childNodes.length > 0):
                    default = defaultTags[0].childNodes[0].data

                if (default == ""):
                    if (type == "Bool"):
                        default = "false"
                    elif (type == "Int"):
                        default = "0"
                    elif (type == "StringList"):
                        default = "empty list"
                    elif (type == "String"):
                        default = "empty string"
                    else:
                        default = "null"

                labelTags = entry.getElementsByTagName("label")
                if (labelTags.length > 0 and labelTags[0].childNodes.length > 0):
                    description = labelTags[0].childNodes[0].data

                print("* ``%s`` (%s, default %s) %s" % (name , type, default, description), file=f)
    except IOError:
        if DEBUG:
            sys.stderr.write("No config in " + plasmoid +"\n")
    #abort on other errors so we can find them

if __name__ == "__main__":
    f = open("plasma_desktop_scripting/keys", "w")

    parseProjectDir('modules/plasma-workspace/applets', f)
    parseProjectDir('modules/plasma-desktop/applets', f)
    parseProjectDir('modules/kdeplasma-addons/applets', f)

    f.close()

