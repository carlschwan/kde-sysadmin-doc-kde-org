# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

import sphinx_rtd_theme

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))


# -- Project information -----------------------------------------------------

project = 'KDE System Administration Documentation'
copyright = '2019, KDE developers'
author = 'KDE developers'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx_rtd_theme',
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = "sphinx_rtd_theme"

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

# -- Processing

import os
from git import Repo
import sys
sys.path.insert(0, os.path.abspath('.'))
import subprocess

if not os.path.exists('modules'):
    os.makedirs('modules')

def clone_or_fetch(repo):
    if not os.path.exists('modules/'+ repo):
        Repo.clone_from('https://anongit.kde.org/' + repo + '.git', 'modules/' + repo)
    else:
        repo = Repo('modules/' + repo)
        for remote in repo.remotes:
            remote.fetch()

clone_or_fetch('plasma-workspace')
clone_or_fetch('plasma-desktop')
clone_or_fetch('kdeplasma-addons')

subprocess.run(['python3', 'extractconfig.py'], check=True)

