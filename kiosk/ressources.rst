Additional Resources
====================

IRC/Matrix Channels
-------------------

* Advice and answers to questions can be found on #kde and #kde-devel on irc.freenode.net

Mailing Lists
-------------

* `KDE Enterprise mailing list and archives <https://mail.kde.org/mailman/listinfo/kde-enterprise>`
* `Kiosk mailing list archives - historical <https://mail.kde.org/mailman/listinfo/kde-kiosk>`
