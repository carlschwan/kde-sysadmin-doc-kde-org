Kiosk Keys
==========

This article contains a listing of known keys that can be used with
Kiosk and what they do. How to actually use these keys and other
capabilities of Kiosk such as URL restrictions, creating assigning
profiles, etc. is covered in the `Introduction to
Kiosk <introduction.html>`__ article.

Which configuration file to put these entries in depends on whether you
wish to make them global to all applications or specific to one
application. To make the restrictions valid for all applications, put
them in kdeglobals. To enable a restriction for a specific applications
place them in the application-specific configuration, e.g. konquerorrc
for Konqueror.

