User & Group Profiles
=====================

.. toctree::
   :titlesonly:
   :hidden:

   introduction
   keys
   ressources


:doc:`introduction`
-------------------

The Kiosk framework provides a set of features that makes it possible to easily and powerfully restrict the capabilities of a KDE environment based on user and group credentials. In addition to an introductory overview, this article covers configuration setting lock down, action and resource restrictions, assigning profiles to users and groups and more.

:doc:`keys`
-----------

This document details known global and application-specific kiosk keys for action, resource and URL restrictions, making it a handy reference guide when setting up a Kiosk profile.

:doc:`ressources`
-----------------

Links to tools, mailing lists and additional documentation relevant to user and group profiles in KDE.
