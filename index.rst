Welcome to KDE System Administration's Documentation
===================================================================

.. toctree::
   :titlesonly:
   :hidden:

   file_system/index
   kiosk/index
   tools/index
   plasma_desktop_scripting/index

This section provides system administrators who are rolling out new or
managing existing KDE deployments in their organization with the
information they need to do so effectively.


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
