## Installation

```bash
pip install sphinx_rtd_theme sphinx gitpython --user
```

## Build

```bash
make html
xdg-open _build/html/index.html
```
