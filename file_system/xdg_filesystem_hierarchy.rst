XDG Filesystem Hierarchy
========================

Freedesktop.org and Standard Directories
----------------------------------------

Many of the free and open source software projects that focus on desktop technology collaborate on shared specifications and software within the [http://freedesktop.org freedesktop.org] project.

One of these shared specifications is the [http://standards.freedesktop.org/basedir-spec/latest/ base directory specification] which defines a set of locations in the filesystem that can be used for portable desktop technology implementations.

KDE supports the base directory specification and the various [[../Environment_Variables|environment variables]] it prescribes in addition to the [[../KDE_Filesystem_Hierarchy|KDE filesystem hierarchy]]. A summary of KDE's implementation follows below, while the full specification can be read [http://standards.freedesktop.org/basedir-spec/latest/ here] on [http://freedesktop.org freedesktop.org].

Application Data
----------------

Data that applications store which are specific to a given user are to be stored in the ``XDG_DATA_HOME`` directory. If the ``XDG_DATA_HOME`` environment variable isn't set, it defaults to ``$HOME/.local/share`` for local storage and ``/usr/local/share;/usr/share`` for system-wide files.

Some items that may be stored in ``XDG_DATA_HOME`` are files the user has put in the trash, mimetype definitions and associations, and application .desktop files.

The search path for system-wide directories can be defined by setting the ``XDG_DATA_DIRS`` environment variable to contain a list of paths.

Configuration
-------------

Configuration information should be stored in the ``XDG_CONFIG_HOME`` directory. If the ``XDG_CONFIG_HOME`` environment variable isn't set, it defaults to ``$HOME/.config`` for local storage and ``/etc/xdg`` for system-wide files.

Some items that may be stored in ``XDG_CONFIG_HOME`` are application configuration files and application menu structure.

The search path for system-wide directories can be defined setting the ``XDG_CONFIG_DIRS`` environment variable to contain a list of paths.

Cache Files
-----------

Temporary data caches can be stored in the ``XDG_CACHE_HOME`` location, which defaults to ``$HOME/.cache``. There is no system-wide cache location, as such caches are always user-specific.

Thumbnails
----------

Thumbnails representing the contents of files for use in icons and other representations of files are to be stored in ``$HOME/.thumbnails``. The structure and content of the thumbnails directory is defined in the [http://jens.triq.net/thumbnail-spec/index.html thumbnail specification].

Others
------

+----------------------------------+---------------+------------------------------------------------------------------------------+
| Directory                        | KDE resource  | Description                                                                  |
+----------------------------------+---------------+------------------------------------------------------------------------------+
| ``$XDG_CONFIG_HOME``             | config        | Contains configuration files. Configuration files are normally named after   |
|                                  |               | the application they belong to, followed by "rc". There are also files that  |
|                                  |               | are specific to a component and as such referenced by all applications that  |
|                                  |               | use that component. A special case is "kdeglobals": this file is read by all |
|                                  |               | KDE applications.                                                            |
+----------------------------------+---------------+------------------------------------------------------------------------------+
| ``$XDG_CONFIG_HOME/session``     | \-            | This directory is used by session management and is normally only available  |
|                                  |               | under $HOME. At the end of a session, KDE applications store their state     |
|                                  |               | here. The file names start with the name of the application followed by a    |
|                                  |               | number. The session manager "ksmserver" stores references to these numbers   |
|                                  |               | when saving a session in "ksmserverrc".                                      |
+----------------------------------+---------------+------------------------------------------------------------------------------+
| ``$XDG_CONFIG_HOME/menus``       | ???           | Contains .menu files describing the KDE menu.                                |
+----------------------------------+---------------+------------------------------------------------------------------------------+
| ``$XDG_DATA_HOME``               | ???           | Contains application-specific data files. Each application has a             |
|                                  |               | subdirectory here for storing its files.                                     |
+----------------------------------+---------------+------------------------------------------------------------------------------+
| ``$XDG_DATA_HOME/doc/HTML``      | ???           | Documentation of KDE applications is stored here. Documentation is           |
|                                  |               | categorized by language and the application it belongs to.                   |
|                                  |               | Normally, at least two files can be found in a directory: "index.docbook",   |
|                                  |               | which contains the documentation in the unformatted docbook format, and      |
|                                  |               | "index.cache.bz2", which contains the same documentation formatted as bzip2  |
|                                  |               | compressed HTML. The HTML version is used by khelpcenter; if the HTML        |
|                                  |               | version is missing, it will regenerate it from the docbook version, but this |
|                                  |               | is a time-consuming process.                                                 |
+----------------------------------+---------------+------------------------------------------------------------------------------+
| ``$XDG_DATA_HOME/icons/``        | icons         | Icons are stored under this directory, categorized by theme, dimension and   |
|                                  |               | usage category.                                                              |
+----------------------------------+---------------+------------------------------------------------------------------------------+
| ``$XDG_DATA_HOME/wallpapers/``   | wallpaper     | This directory contains images that can be used as background pictures.      |
+----------------------------------+---------------+------------------------------------------------------------------------------+
| ``$XDG_DATA_HOME/templates/``    | template      | This directory contains templates for creating files of various types. A     |
|                                  |               | template consists of a ``.desktop`` file that describes the file and         |
|                                  |               | includes a reference to a file in the ``.source`` subdirectory. The          |
|                                  |               | templates in this directory appear in the "Create New" menu available on the |
|                                  |               | desktop and in the file browser. When a user selects a template from the     |
|                                  |               | menu, its source file is copied.                                             |
+----------------------------------+---------------+------------------------------------------------------------------------------+
