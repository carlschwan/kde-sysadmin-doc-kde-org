File System
===========

.. toctree::
   :titlesonly:
   :hidden:

   configuration_files
   xdg_filesystem_hierarchy

:doc:`configuration_files`
--------------------------

KDE configuration files are text-based. They contain groups of key-value pairs. This section explains their syntax. It covers:

    * localization,
    * shell variables,
    * complete or partial lock-down of configuration files,
    * editor utilities,
    * other advanced features.

:doc:`xdg_filesystem_hierarchy`
-------------------------------

This article describes how KDE uses the filesystem, where it looks for files, and where it stores them. It explains how to change these locations. There is also a brief overview of the default settings used by major OS vendors.

