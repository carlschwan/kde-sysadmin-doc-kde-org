File Format
===========

Introduction
------------

KDE uses a simple text-based file format for all its configuration files.
It consists of key-value pairs that are placed in groups. All KDE
configuration files use UTF-8 encoding for text outside the ASCII range.

The start of a group is indicated by a group name that is placed in
square brackets. All the key-value entries that follow belong to the group.
The group ends when either another group starts or when the end of the file
is reached.

Entries at the top of the file that are not preceded by a group name belong
to the default group.

Example: A Simple Config File
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The following example shows a configuration file that consists of two groups.
The first group contains the keys ``LargeCursor`` and
``SingleClick``, the second group contains the keys
``Show hidden files`` and ``Sort by``.

.. code-block:: ini

    [KDE]
    LargeCursor=false
    SingleClick=true

    [KFileDialog Settings]
    Show hidden files=false
    Sort by=Name

Entries in a group consists of a key and value separated by an equal sign.
The key can contain spaces and may be followed by options placed in
square brackets.

The part after the equal sign is the value of the entry. Any white space
surrounding the equal sign is ignored, as is any trailing white space.

If a value is supposed to include a space at the begin or end then this can be achieved
by using a backslash followed by an 's'.

There are several other backslash codes, here is the complete list:

* ``\s`` can be used as space
* ``\t`` can be used to include a tab
* ``\r`` for a carriage return character
* ``\n`` for a linefeed character (new line)
* ``\\`` to include the backslash itself

Example: Whitespace
~~~~~~~~~~~~~~~~~~~

The following config file fragment is equivalent to the one in the
previous example. Whitespace surrounding the key and value is ignored.

.. code-block:: ini

    [KDE]
    LargeCursor=          false
    SingleClick = true

    [KFileDialog Settings]
       Show hidden files   = false
    Sort by =Name

Example: Preserving Whitespace
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In the following example the value of the ``Caption`` entry starts
with two spaces while the ``Description`` entry contains three lines
of text. Linefeeds in backslash notation are used to separate the different lines.

.. code-block:: ini

    [Preview Image]
    Caption=\s My Caption
    Description=This is\na very long\ndescription.

Empty lines in configuration files are ignored, just as lines that
start with a hash mark '#'. The hash mark can be used to add comments to
configuration files but it should be noted that when a KDE application
updates a configuration file the comments are not
preserved.

Configuration
-------------
There can be multiple configuration files with the same name in the
share/config sub-directory of the various :ref:`kde-directory-trees`.
In that case the information of all these configuration files is combined
on a key-by-key basis. If the same key within a certain group is defined
in more than one place, the key value read from the directory tree with
the highest precedence will be used.
Configuration files under $KDEHOME always have the highest precedence.
If a key in a certain group is defined multiple times in a single file, the
value of the last entry is used.

KDE's cascading configuration scheme can be used to provide users with
system or organisation-wide default settings while still allowing the users
to make individual changes to these settings. KDE will not write entries to
the user's configuration file under $KDEHOME that match any default settings
provided this way. This way changes made to the default settings will
immediately be propagated to the user. Note that differs from the traditional
way of providing default settings for users through the use of ``/etc/skel``,
changes to ``/etc/skel`` will only be propagated when creating a new user account.

When it is undesirable that individual users can make changes to default
settings, the default setting can be locked down.

Shell Expansion
---------------

So called ''Shell Expansion'' can be used to provide more dynamic default
values. With shell expansion the value of a configuration key can be
constructed from the value of an environment variable.

To enable shell expansion for a configuration entry, the key must be followed
by ``[$e]``. Normally the expanded form is written into the users
configuration file after first use. To prevent that, it is recommend to lock
the configuration entry down by using ``[$ie]``.

Example: Dynamic Entries
~~~~~~~~~~~~~~~~~~~~~~~~

The value for the "``Email``" entry is determined by filling in
the values of the $USER and $HOST environment variables. When ''joe''
is logged in on ''joes_host'' this will result in a value equal to
"``joe@joes_host``". The setting is not locked down.

.. code-block:: ini

    [Mail Settings]
    Email[$e]=${USER}@${HOST}

Localization
------------

All configuration entries can be indexed with a language code. In this case,
the language that the user has selected for use on the desktop is used to
look up the key value. If the default language (American English) has been
selected or if there is no index that corresponds to the selected language,
the key entry without index is used.

Example: UTF-8 In Keys
~~~~~~~~~~~~~~~~~~~~~~

In the following example the value of the "``Caption``" entry
depends on the language. If the user has selected french as language (language
code ``fr``) the value of the entry will be "``Ma Légende``".
In all other cases the value "``My Caption``" will be used.

.. code-block:: ini

    [Preview Image]
    Caption=My Caption
    Caption[fr]=Ma Légende

Lock Down
---------

To prevent users from overriding default settings, you can lock
these settings down in the system-wide config files. Settings can be locked down individually, per group,
or per file. An individual entry can be locked down by adding ``[$i]``
behind the key. A group of entries can be locked down by placing
``[$i]`` behind the group name. To lock down the entire file, start
the file with ``[$i]`` on a single line.

Example: Using [$i]
~~~~~~~~~~~~~~~~~~~

In the following example the "``Caption``" entry has been locked
down. All entries in the "``Mail Settings``" group have been
locked down as well.

.. code-block:: ini

    [Preview Image]
    Caption[$i]=My Caption

    [Mail Settings][$i]
    Host[$e]=$(hostname)
    Email=wal&#100;o&#x40;&#00107;&#x64;e&#x2e;or&#x67;

If an entry has been locked down, entries that would otherwise take
precedence over the default setting will now be ignored.

Any changes that applications make to settings that have been locked
down will be silently dropped. Applications may respond differently to locked down settings. Some applications
recognize when a setting has been locked down and will remove the
elements from its user interface. Other applications will continue
to offer these options in their user interface even though the option
does not have any effect any more. It is also possible that it is actually
still possible to change the setting in the running program. In that case the
changed setting will be effective for the time the program remains running.
When the program is restarted it will then return to the locked-down default
settings.

Editors
-------

For scripts or modifying files from the command line, the command line app ``kwriteconfig5`` is very useful. ``kwriteconfig5`` expects a configuration file name, a group, a key, an optional type and a value, such as:

.. code-block:: sh

    kwriteconfig5 --file kickerrc \
                 --group General \
                 --key AutoHidePanel \
                 --type bool \
                 true

